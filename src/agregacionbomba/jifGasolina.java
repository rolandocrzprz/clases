/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

import javax.swing.JOptionPane;

/**
 *
 * @author Rolando Cruz
 */
public class jifGasolina extends javax.swing.JInternalFrame {

    /**
     * Creates new form jifGasolina
     */
    public jifGasolina() {
        initComponents();
        this.resize(1000,800);
        ga = new Gasolina();
        bom = new Bomba();
        
        this.txtCantidad.setEnabled(false);
        this.btnHacerVenta.setEnabled(false);
        this.txtContador.setEnabled(false);
    }

   
    public void deshabilitar () {
        this.txtNumBomba.setEnabled(false);
        this.cmbTipo.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.btnIniciar.setEnabled(false);
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        txtPrecio = new javax.swing.JTextField();
        txtContador = new javax.swing.JTextField();
        JslCapacidad = new javax.swing.JSlider();
        jLabel6 = new javax.swing.JLabel();
        txtNumBomba = new javax.swing.JTextField();
        btnIniciar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblMensaje = new javax.swing.JLabel();
        btnHacerVenta = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtTotalVenta = new javax.swing.JTextField();

        setBackground(new java.awt.Color(153, 153, 153));
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(0, 153, 102));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PEMEX", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel1.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tipo de Gasolina");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(250, 40, 120, 20);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Contador de Litros");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(420, 40, 130, 20);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Precio de Venta");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(440, 80, 108, 20);

        cmbTipo.setBackground(new java.awt.Color(255, 255, 255));
        cmbTipo.setForeground(new java.awt.Color(0, 0, 0));
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Regular", "Premium" }));
        jPanel1.add(cmbTipo);
        cmbTipo.setBounds(250, 70, 100, 30);

        txtPrecio.setBackground(new java.awt.Color(255, 255, 255));
        txtPrecio.setForeground(new java.awt.Color(0, 0, 0));
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        jPanel1.add(txtPrecio);
        txtPrecio.setBounds(570, 80, 73, 24);

        txtContador.setBackground(new java.awt.Color(255, 255, 255));
        txtContador.setForeground(new java.awt.Color(0, 0, 0));
        txtContador.setText("0");
        txtContador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContadorActionPerformed(evt);
            }
        });
        jPanel1.add(txtContador);
        txtContador.setBounds(570, 40, 70, 24);

        JslCapacidad.setBackground(new java.awt.Color(51, 204, 0));
        JslCapacidad.setForeground(new java.awt.Color(0, 0, 0));
        JslCapacidad.setMajorTickSpacing(10);
        JslCapacidad.setPaintLabels(true);
        JslCapacidad.setPaintTicks(true);
        jPanel1.add(JslCapacidad);
        JslCapacidad.setBounds(50, 126, 740, 50);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Num. Bomba");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(30, 40, 100, 20);

        txtNumBomba.setBackground(new java.awt.Color(255, 255, 255));
        txtNumBomba.setForeground(new java.awt.Color(0, 0, 0));
        txtNumBomba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumBombaActionPerformed(evt);
            }
        });
        jPanel1.add(txtNumBomba);
        txtNumBomba.setBounds(30, 70, 90, 30);

        btnIniciar.setBackground(new java.awt.Color(255, 255, 255));
        btnIniciar.setForeground(new java.awt.Color(0, 0, 0));
        btnIniciar.setText("Iniciar Bomba");
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });
        jPanel1.add(btnIniciar);
        btnIniciar.setBounds(690, 30, 120, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(50, 70, 850, 200);

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));
        jPanel2.setLayout(null);

        lblMensaje.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMensaje.setForeground(new java.awt.Color(255, 255, 255));
        lblMensaje.setText("Mensaje");
        jPanel2.add(lblMensaje);
        lblMensaje.setBounds(40, 130, 550, 80);

        btnHacerVenta.setBackground(new java.awt.Color(255, 255, 255));
        btnHacerVenta.setForeground(new java.awt.Color(0, 0, 0));
        btnHacerVenta.setText("Hacer la Venta");
        btnHacerVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHacerVentaActionPerformed(evt);
            }
        });
        jPanel2.add(btnHacerVenta);
        btnHacerVenta.setBounds(460, 30, 150, 40);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Cantidad:");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(30, 40, 70, 30);

        txtCantidad.setBackground(new java.awt.Color(255, 255, 255));
        txtCantidad.setForeground(new java.awt.Color(0, 0, 0));
        txtCantidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        jPanel2.add(txtCantidad);
        txtCantidad.setBounds(120, 40, 90, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(80, 380, 810, 220);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("VENTA DE GASOLINA");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(360, 320, 270, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Total Venta:    $");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(550, 630, 132, 30);

        txtTotalVenta.setBackground(new java.awt.Color(255, 255, 255));
        txtTotalVenta.setForeground(new java.awt.Color(0, 0, 0));
        txtTotalVenta.setText("0.0");
        txtTotalVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalVentaActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalVenta);
        txtTotalVenta.setBounds(690, 630, 90, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void txtContadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContadorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtContadorActionPerformed

    private void txtNumBombaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumBombaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumBombaActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void txtTotalVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalVentaActionPerformed


    }//GEN-LAST:event_txtTotalVentaActionPerformed

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        
        // TODO add your handling code here:
        boolean exito = false;
        if (this.txtNumBomba.getText().equals("")) {
            exito = true;
        }
        if (this.txtContador.getText().equals("")) {
            exito = true;
        }
        if (this.txtPrecio.getText().equals("")) {
            exito = true;
        }
        if (exito == true) {
            //falto informacion
            JOptionPane.showMessageDialog(this, "Falto capturar informacion");
        } else {
            bom.setNumBomba(Integer.parseInt(this.txtNumBomba.getText()));
            ga.setPrecio(Integer.parseInt(this.txtPrecio.getText()));
            bom.setCapacidad(this.JslCapacidad.getValue());
//               JslCapacidad.setValue(Integer.parseInt(this.txtContador.getText()));

            switch (this.cmbTipo.getSelectedIndex()) {
                case 0:
                    ga.setTipo(0);
                    break;
                case 1:
                    ga.setTipo(1);
                    break;
            }
            JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
            this.btnHacerVenta.setEnabled(true);
            this.txtCantidad.setEnabled(true);
            
            
            this.deshabilitar();
        }

       
    }//GEN-LAST:event_btnIniciarActionPerformed

    private void btnHacerVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHacerVentaActionPerformed
        // TODO add your handling code here:
        
        if (txtCantidad.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Ingrese una cantidad");
            return;
        }

        float cantidadVenta = Float.parseFloat(txtCantidad.getText());

        if (!bom.realizarVenta(cantidadVenta)) {
            JOptionPane.showMessageDialog(this, "No hay suficiente Gasolina");
            return;
        }

        float totalVenta = cantidadVenta * ga.getPrecio();
        float totalActual = Float.parseFloat(txtTotalVenta.getText());
        float nuevoTotal = totalActual + totalVenta;
        txtTotalVenta.setText(String.valueOf(nuevoTotal));

        bom.setContador(bom.getContador() + cantidadVenta);
        txtContador.setText(String.valueOf(bom.getContador()));
        JslCapacidad.setValue((int) (JslCapacidad.getValue() - cantidadVenta));

        lblMensaje.setText("Venta realizada con éxito. El costo de su venta es:  $ " + totalVenta);
        
    }//GEN-LAST:event_btnHacerVentaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSlider JslCapacidad;
    private javax.swing.JButton btnHacerVenta;
    private javax.swing.JButton btnIniciar;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblMensaje;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtContador;
    private javax.swing.JTextField txtNumBomba;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotalVenta;
    // End of variables declaration//GEN-END:variables

    private Gasolina ga;
    private Bomba bom;
}
