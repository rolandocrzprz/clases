/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Rolando Cruz
 */
public class EmpleadoBase extends Empleado implements Impuesto {

    private float pagodia;
    private float diaTra;

    public EmpleadoBase() {
        this.pagodia = 0.0f;
        this.diaTra = 0.0f;
    }

    public EmpleadoBase(float pagodia, float diaTra, int numEmp, String nomEmp, String puesto, String departamento) {
        super(numEmp, nomEmp, puesto, departamento);
        this.pagodia = pagodia;
        this.diaTra = diaTra;
    }

    public float getPagodia() {
        return pagodia;
    }

    public void setPagodia(float pagodia) {
        this.pagodia = pagodia;
    }

    public float getDiaTra() {
        return diaTra;
    }

    public void setDiaTra(float diaTra) {
        this.diaTra = diaTra;
    }

    @Override
    public float calcularPago() {
        return this.pagodia * this.diaTra;
    }

    @Override
    public float calcularImpuesto() {
        float impuestos = 0.0f;
        if (this.calcularPago() > 5000) {
            impuestos = this.calcularPago() * .16f;
        }
        return impuestos;
        
        
    }

}
