/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Rolando Cruz
 */
public abstract class Empleado {
    
    protected int numEmp;
    protected String nomEmp;
    protected String puesto;
    protected String departamento;

    public Empleado() {
        this.numEmp = 0;
        this.nomEmp = "";
        this.puesto = "";
        this.departamento = "";
    }

    public Empleado(int numEmp, String nomEmp, String puesto, String departamento) {
        this.numEmp = numEmp;
        this.nomEmp = nomEmp;
        this.puesto = puesto;
        this.departamento = departamento;
    }

    public int getNumEmp() {
        return numEmp;
    }

    public void setNumEmp(int numEmp) {
        this.numEmp = numEmp;
    }

    public String getNomEmp() {
        return nomEmp;
    }

    public void setNomEmp(String nomEmp) {
        this.nomEmp = nomEmp;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    
    
    public abstract float calcularPago();
    
}
