/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Rolando Cruz
 */
public class EmpleadoEventual extends Empleado {

    private float pagoHora;
    private float horasTra;

    public EmpleadoEventual() {
        this.pagoHora = 0.0f;
        this.horasTra = 0.0f;
    }

    public EmpleadoEventual(float pagoHora, float horasTra, int numEmp, String nomEmp, String puesto, String departamento) {
        super(numEmp, nomEmp, puesto, departamento);
        this.pagoHora = pagoHora;
        this.horasTra = horasTra;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHorasTra() {
        return horasTra;
    }

    public void setHorasTra(float horasTra) {
        this.horasTra = horasTra;
    }

    @Override
    public float calcularPago() {
        return this.horasTra * this.pagoHora;
    }

}
