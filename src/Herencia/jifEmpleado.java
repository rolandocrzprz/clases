/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

import javax.swing.JOptionPane;

/**
 *
 * @author Rolando Cruz
 */
public class jifEmpleado extends javax.swing.JInternalFrame {

    /**
     * Creates new form jifEmpleado
     */
    public jifEmpleado() {
        initComponents();
        this.deshabilitar();
    }

    public void deshabilitar() {
        this.txtNumEmp.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtPuesto.setEnabled(false);
        this.txtDepartamento.setEnabled(false);
        this.txtPagoDia.setEnabled(false);
        this.txtDiasTra.setEnabled(false);

        this.btnGuardar.setEnabled(false);
        this.btnMostrar.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        this.btnLimpiar.setEnabled(false);

        this.txtImpuesto.setEnabled(false);
        this.txtPago.setEnabled(false);
        this.txtTotalPago.setEnabled(false);
    }

    public void habilitar() {
        this.txtNumEmp.setEnabled(!false);
        this.txtNombre.setEnabled(!false);
        this.txtPuesto.setEnabled(!false);
        this.txtDepartamento.setEnabled(!false);
        this.txtPagoDia.setEnabled(!false);
        this.txtDiasTra.setEnabled(!false);

        this.btnGuardar.setEnabled(!false);
        this.btnMostrar.setEnabled(!false);
        this.btnCancelar.setEnabled(!false);
        this.btnLimpiar.setEnabled(!false);;
    }

    public void limpiar() {
        this.txtNumEmp.setText("");
        this.txtNombre.setText("");
        this.txtPuesto.setText("");
        this.txtDepartamento.setText("");
        this.txtPagoDia.setText("");
        this.txtDiasTra.setText("");

        this.txtImpuesto.setText("");
        this.txtPago.setText("");
        this.txtTotalPago.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumEmp = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtDepartamento = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPuesto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtPagoDia = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtDiasTra = new javax.swing.JTextField();
        btnMostrar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextField17 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jTextField21 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        txtImpuesto = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtPago = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txtTotalPago = new javax.swing.JTextField();

        jPanel1.setBackground(new java.awt.Color(153, 153, 153));
        jPanel1.setLayout(null);

        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Num. Empleado:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 20, 100, 20);

        txtNumEmp.setBackground(new java.awt.Color(255, 255, 255));
        txtNumEmp.setForeground(new java.awt.Color(0, 0, 0));
        txtNumEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumEmpActionPerformed(evt);
            }
        });
        jPanel1.add(txtNumEmp);
        txtNumEmp.setBounds(110, 20, 80, 24);

        txtNombre.setBackground(new java.awt.Color(255, 255, 255));
        txtNombre.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(txtNombre);
        txtNombre.setBounds(110, 50, 80, 24);

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Nombre:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(30, 50, 60, 20);

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Departamento:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 110, 90, 20);

        txtDepartamento.setBackground(new java.awt.Color(255, 255, 255));
        txtDepartamento.setForeground(new java.awt.Color(0, 0, 0));
        txtDepartamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDepartamentoActionPerformed(evt);
            }
        });
        jPanel1.add(txtDepartamento);
        txtDepartamento.setBounds(110, 110, 80, 24);

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Puesto:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 80, 50, 20);

        txtPuesto.setBackground(new java.awt.Color(255, 255, 255));
        txtPuesto.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(txtPuesto);
        txtPuesto.setBounds(110, 80, 80, 24);

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Pago Por Dia:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(210, 20, 80, 20);

        txtPagoDia.setBackground(new java.awt.Color(255, 255, 255));
        txtPagoDia.setForeground(new java.awt.Color(0, 0, 0));
        txtPagoDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoDiaActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoDia);
        txtPagoDia.setBounds(290, 20, 80, 24);

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Dias Trabajados:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(200, 80, 100, 20);

        txtDiasTra.setBackground(new java.awt.Color(255, 255, 255));
        txtDiasTra.setForeground(new java.awt.Color(0, 0, 0));
        txtDiasTra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiasTraActionPerformed(evt);
            }
        });
        jPanel1.add(txtDiasTra);
        txtDiasTra.setBounds(300, 80, 80, 24);

        btnMostrar.setBackground(new java.awt.Color(102, 102, 102));
        btnMostrar.setForeground(new java.awt.Color(255, 255, 255));
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnMostrar);
        btnMostrar.setBounds(440, 100, 90, 32);

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));
        jPanel2.setLayout(null);

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Num. Empleado:");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(10, 20, 100, 20);

        jTextField7.setBackground(new java.awt.Color(255, 255, 255));
        jTextField7.setForeground(new java.awt.Color(0, 0, 0));
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField7);
        jTextField7.setBounds(110, 20, 80, 24);

        jTextField8.setBackground(new java.awt.Color(255, 255, 255));
        jTextField8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jTextField8);
        jTextField8.setBounds(110, 50, 80, 24);

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Nombre:");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(30, 50, 60, 20);

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Departamento:");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(20, 110, 90, 20);

        jTextField9.setBackground(new java.awt.Color(255, 255, 255));
        jTextField9.setForeground(new java.awt.Color(0, 0, 0));
        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField9);
        jTextField9.setBounds(110, 110, 80, 24);

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("Puesto:");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(30, 80, 50, 20);

        jTextField10.setBackground(new java.awt.Color(255, 255, 255));
        jTextField10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jTextField10);
        jTextField10.setBounds(110, 80, 80, 24);

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Pago Por Dia:");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(210, 20, 80, 20);

        jTextField11.setBackground(new java.awt.Color(255, 255, 255));
        jTextField11.setForeground(new java.awt.Color(0, 0, 0));
        jTextField11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField11ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField11);
        jTextField11.setBounds(290, 20, 80, 24);

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("Dias Trabajados:");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(200, 80, 100, 20);

        jTextField12.setBackground(new java.awt.Color(255, 255, 255));
        jTextField12.setForeground(new java.awt.Color(0, 0, 0));
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField12);
        jTextField12.setBounds(300, 80, 80, 24);

        jButton2.setBackground(new java.awt.Color(102, 102, 102));
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Nuevo");
        jPanel2.add(jButton2);
        jButton2.setBounds(510, 50, 90, 32);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 0, 0);

        btnNuevo.setBackground(new java.awt.Color(102, 102, 102));
        btnNuevo.setForeground(new java.awt.Color(255, 255, 255));
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel1.add(btnNuevo);
        btnNuevo.setBounds(440, 20, 90, 32);

        btnGuardar.setBackground(new java.awt.Color(102, 102, 102));
        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnGuardar);
        btnGuardar.setBounds(440, 60, 90, 32);

        jPanel3.setBackground(new java.awt.Color(153, 153, 153));
        jPanel3.setLayout(null);

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Num. Empleado:");
        jPanel3.add(jLabel13);
        jLabel13.setBounds(10, 20, 100, 20);

        jTextField13.setBackground(new java.awt.Color(255, 255, 255));
        jTextField13.setForeground(new java.awt.Color(0, 0, 0));
        jTextField13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField13ActionPerformed(evt);
            }
        });
        jPanel3.add(jTextField13);
        jTextField13.setBounds(110, 20, 80, 24);

        jTextField14.setBackground(new java.awt.Color(255, 255, 255));
        jTextField14.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(jTextField14);
        jTextField14.setBounds(110, 50, 80, 24);

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Nombre:");
        jPanel3.add(jLabel14);
        jLabel14.setBounds(30, 50, 60, 20);

        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Departamento:");
        jPanel3.add(jLabel15);
        jLabel15.setBounds(20, 110, 90, 20);

        jTextField15.setBackground(new java.awt.Color(255, 255, 255));
        jTextField15.setForeground(new java.awt.Color(0, 0, 0));
        jTextField15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField15ActionPerformed(evt);
            }
        });
        jPanel3.add(jTextField15);
        jTextField15.setBounds(110, 110, 80, 24);

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Puesto:");
        jPanel3.add(jLabel16);
        jLabel16.setBounds(30, 80, 50, 20);

        jTextField16.setBackground(new java.awt.Color(255, 255, 255));
        jTextField16.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.add(jTextField16);
        jTextField16.setBounds(110, 80, 80, 24);

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Pago Por Dia:");
        jPanel3.add(jLabel17);
        jLabel17.setBounds(210, 20, 80, 20);

        jTextField17.setBackground(new java.awt.Color(255, 255, 255));
        jTextField17.setForeground(new java.awt.Color(0, 0, 0));
        jTextField17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField17ActionPerformed(evt);
            }
        });
        jPanel3.add(jTextField17);
        jTextField17.setBounds(290, 20, 80, 24);

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Dias Trabajados:");
        jPanel3.add(jLabel18);
        jLabel18.setBounds(200, 80, 100, 20);

        jTextField18.setBackground(new java.awt.Color(255, 255, 255));
        jTextField18.setForeground(new java.awt.Color(0, 0, 0));
        jTextField18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField18ActionPerformed(evt);
            }
        });
        jPanel3.add(jTextField18);
        jTextField18.setBounds(300, 80, 80, 24);

        jButton5.setBackground(new java.awt.Color(102, 102, 102));
        jButton5.setForeground(new java.awt.Color(255, 255, 255));
        jButton5.setText("Mostrar");
        jPanel3.add(jButton5);
        jButton5.setBounds(460, 100, 90, 32);

        jPanel4.setBackground(new java.awt.Color(153, 153, 153));
        jPanel4.setLayout(null);

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("Num. Empleado:");
        jPanel4.add(jLabel19);
        jLabel19.setBounds(10, 20, 100, 20);

        jTextField19.setBackground(new java.awt.Color(255, 255, 255));
        jTextField19.setForeground(new java.awt.Color(0, 0, 0));
        jTextField19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField19ActionPerformed(evt);
            }
        });
        jPanel4.add(jTextField19);
        jTextField19.setBounds(110, 20, 80, 24);

        jTextField20.setBackground(new java.awt.Color(255, 255, 255));
        jTextField20.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(jTextField20);
        jTextField20.setBounds(110, 50, 80, 24);

        jLabel20.setForeground(new java.awt.Color(0, 0, 0));
        jLabel20.setText("Nombre:");
        jPanel4.add(jLabel20);
        jLabel20.setBounds(30, 50, 60, 20);

        jLabel21.setForeground(new java.awt.Color(0, 0, 0));
        jLabel21.setText("Departamento:");
        jPanel4.add(jLabel21);
        jLabel21.setBounds(20, 110, 90, 20);

        jTextField21.setBackground(new java.awt.Color(255, 255, 255));
        jTextField21.setForeground(new java.awt.Color(0, 0, 0));
        jTextField21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField21ActionPerformed(evt);
            }
        });
        jPanel4.add(jTextField21);
        jTextField21.setBounds(110, 110, 80, 24);

        jLabel22.setForeground(new java.awt.Color(0, 0, 0));
        jLabel22.setText("Puesto:");
        jPanel4.add(jLabel22);
        jLabel22.setBounds(30, 80, 50, 20);

        jTextField22.setBackground(new java.awt.Color(255, 255, 255));
        jTextField22.setForeground(new java.awt.Color(0, 0, 0));
        jPanel4.add(jTextField22);
        jTextField22.setBounds(110, 80, 80, 24);

        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("Pago Por Dia:");
        jPanel4.add(jLabel23);
        jLabel23.setBounds(210, 20, 80, 20);

        jTextField23.setBackground(new java.awt.Color(255, 255, 255));
        jTextField23.setForeground(new java.awt.Color(0, 0, 0));
        jTextField23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField23ActionPerformed(evt);
            }
        });
        jPanel4.add(jTextField23);
        jTextField23.setBounds(290, 20, 80, 24);

        jLabel24.setForeground(new java.awt.Color(0, 0, 0));
        jLabel24.setText("Dias Trabajados:");
        jPanel4.add(jLabel24);
        jLabel24.setBounds(200, 80, 100, 20);

        jTextField24.setBackground(new java.awt.Color(255, 255, 255));
        jTextField24.setForeground(new java.awt.Color(0, 0, 0));
        jTextField24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField24ActionPerformed(evt);
            }
        });
        jPanel4.add(jTextField24);
        jTextField24.setBounds(300, 80, 80, 24);

        jButton6.setBackground(new java.awt.Color(102, 102, 102));
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setText("Nuevo");
        jPanel4.add(jButton6);
        jButton6.setBounds(510, 50, 90, 32);

        jPanel3.add(jPanel4);
        jPanel4.setBounds(0, 0, 0, 0);

        jButton7.setBackground(new java.awt.Color(102, 102, 102));
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText("Nuevo");
        jPanel3.add(jButton7);
        jButton7.setBounds(460, 20, 90, 32);

        jButton8.setBackground(new java.awt.Color(102, 102, 102));
        jButton8.setForeground(new java.awt.Color(255, 255, 255));
        jButton8.setText("Guardar");
        jPanel3.add(jButton8);
        jButton8.setBounds(460, 60, 90, 32);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 0, 0, 0);

        btnCerrar.setBackground(new java.awt.Color(102, 102, 102));
        btnCerrar.setForeground(new java.awt.Color(255, 255, 255));
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCerrar);
        btnCerrar.setBounds(320, 340, 90, 32);

        btnCancelar.setBackground(new java.awt.Color(102, 102, 102));
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);
        btnCancelar.setBounds(220, 340, 90, 32);

        btnLimpiar.setBackground(new java.awt.Color(102, 102, 102));
        btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        jPanel1.add(btnLimpiar);
        btnLimpiar.setBounds(120, 340, 90, 32);

        jPanel5.setBackground(new java.awt.Color(153, 204, 255));
        jPanel5.setLayout(null);

        jLabel25.setForeground(new java.awt.Color(0, 0, 0));
        jLabel25.setText("Impuestos:");
        jPanel5.add(jLabel25);
        jLabel25.setBounds(30, 20, 70, 20);

        txtImpuesto.setBackground(new java.awt.Color(255, 255, 255));
        txtImpuesto.setForeground(new java.awt.Color(0, 0, 0));
        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        jPanel5.add(txtImpuesto);
        txtImpuesto.setBounds(100, 20, 80, 24);

        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("Pago:");
        jPanel5.add(jLabel26);
        jLabel26.setBounds(50, 50, 40, 20);

        txtPago.setBackground(new java.awt.Color(255, 255, 255));
        txtPago.setForeground(new java.awt.Color(0, 0, 0));
        txtPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoActionPerformed(evt);
            }
        });
        jPanel5.add(txtPago);
        txtPago.setBounds(100, 50, 80, 24);

        jLabel27.setForeground(new java.awt.Color(0, 0, 0));
        jLabel27.setText("Total Pago:");
        jPanel5.add(jLabel27);
        jLabel27.setBounds(30, 80, 70, 20);

        txtTotalPago.setBackground(new java.awt.Color(255, 255, 255));
        txtTotalPago.setForeground(new java.awt.Color(0, 0, 0));
        txtTotalPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalPagoActionPerformed(evt);
            }
        });
        jPanel5.add(txtTotalPago);
        txtTotalPago.setBounds(100, 80, 80, 24);

        jPanel1.add(jPanel5);
        jPanel5.setBounds(110, 170, 330, 160);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11ActionPerformed

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void txtDiasTraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiasTraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiasTraActionPerformed

    private void txtPagoDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoDiaActionPerformed

    private void txtDepartamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDepartamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDepartamentoActionPerformed

    private void txtNumEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumEmpActionPerformed

    private void jTextField13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField13ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13ActionPerformed

    private void jTextField15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField15ActionPerformed

    private void jTextField17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField17ActionPerformed

    private void jTextField18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField18ActionPerformed

    private void jTextField19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField19ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField19ActionPerformed

    private void jTextField21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField21ActionPerformed

    private void jTextField23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField23ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField23ActionPerformed

    private void jTextField24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField24ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField24ActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = JOptionPane.showConfirmDialog(this, "Deseas salirte", "Empleado", JOptionPane.YES_NO_OPTION);

        if (opcion == JOptionPane.YES_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoActionPerformed

    private void txtTotalPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalPagoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        emp = new EmpleadoBase();
        this.habilitar();

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed

        boolean exito = false;
        if (this.txtNumEmp.getText().equals("")) {
            exito = true;
        }
        if (this.txtNombre.getText().equals("")) {
            exito = true;
        }
        if (this.txtPuesto.getText().equals("")) {
            exito = true;
        }
        if (this.txtDepartamento.getText().equals("")) {
            exito = true;
        }
        if (this.txtPagoDia.getText().equals("")) {
            exito = true;
        }
        if (this.txtDiasTra.getText().equals("")) {
            exito = true;
        }
        if (exito == true) {
            //falto informacion
            JOptionPane.showMessageDialog(this, "Falto capturar informacion");
        } else {

            try {

                emp.setNumEmp(Integer.parseInt(this.txtNumEmp.getText()));
                emp.setNomEmp(this.txtNombre.getText());
                emp.setPuesto(this.txtPuesto.getText());
                emp.setDepartamento(this.txtDepartamento.getText());
                emp.setPagodia(Float.parseFloat(this.txtPagoDia.getText()));
                emp.setDiaTra(Float.parseFloat(this.txtDiasTra.getText()));
            } catch (NumberFormatException e) {
                exito = true;
                JOptionPane.showMessageDialog(this, "Surgio un error " + e.getMessage());

            }
            if (exito == false) JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
            
            this.btnMostrar.setEnabled(true);
        }


    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.txtNumEmp.setText(String.valueOf(emp.getNumEmp()));
        this.txtNombre.setText(String.valueOf(emp.getNomEmp()));
        this.txtPuesto.setText(String.valueOf(emp.getPuesto()));
        this.txtDepartamento.setText(String.valueOf(emp.getDepartamento()));
        this.txtPagoDia.setText(String.valueOf(emp.getPagodia()));
        this.txtDiasTra.setText(String.valueOf(emp.getDiaTra()));

        this.txtImpuesto.setText(String.valueOf(emp.calcularImpuesto()));
        this.txtPago.setText(String.valueOf(emp.calcularPago()));
        this.txtTotalPago.setText(String.valueOf(emp.calcularPago() - emp.calcularImpuesto()));
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField22;
    private javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTextField txtDepartamento;
    private javax.swing.JTextField txtDiasTra;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmp;
    private javax.swing.JTextField txtPago;
    private javax.swing.JTextField txtPagoDia;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotalPago;
    // End of variables declaration//GEN-END:variables
    private EmpleadoBase emp;
}
